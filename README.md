#Simulador de Algoritmos de Substituição de Páginas
 _Contato:adrielsantos@id.uff.br_
 
## Especificação
Construa um simulador de algoritmos de substituição de página de memória em cache. O simulador deve receber como entrada a sequência de referências às páginas de mem[oria principal (endereços), e simular as substituições realizadas em cache após a ocorrência de um miss. para os algoritmos FIFO, LRU, LFU, RANDOM. O programa deve receber como parâmetro a capacidade total da memória cache (ou seja, número total de páginas), o esquema de mapeamento (direto, associativo e associativo por conjunto). A cache vai operar do nome do arquivo de entrada a ser lido pelo programa, contendo as sequências de referências aos acessos de páginas da memória. O formato do arquivo de entrada consiste em um valor de endereço de memória (um número inteiro por linha) a ser carregado no programa. A saída do simulador deve consistir de, para cada política de substituição:
1. A cada nova referência de memória do arquivo de entrada, imprimir a lista de todas as páginas armazenadas na memória cache;
2. Ao final da execução, a fração de acertos às referências de memória para cada política.


## O Simulador

O simulador desenvolvido neste projeto foi projetado utilizando a linguagem de programação Python. Para cada arquivo de entrada o programa solicita como parâmetros o esquema de mapeamento e o tamanho total da memória cache. Após a entrada destas informações, o simulador vai operar sobre o arquivo de entrada e apresentar em tela e em arquivo as saídas solicitadas na especificação deste projeto.

Foram considerados três esquemas de mapeamento: a) Direto; b) Associativo; e c) Associativo por Conjunto. Os mapeamentos Associativo e Associativo por Conjunto consideram quatro políticas diferentes de substituição de páginas: FIFO, LRU, LFU e Random.

Como o tamanho dos conjuntos (para o mapeamento Associativo por Conjunto) não foi descrito na especificação deste projeto, assumiu-se que cada conjunto terá tamanho 2. Portanto, em uma simulação onde, por exemplo, o tamanho informado para a cache seja 16, a simulação irá considerar 8 conjuntos para a cache, cada um de tamanho 2. Para isso, o tamanho informado para a cache, naturalmente, não poderá ser um número ímpar.
<p></p>

---

## Requisitos

- Python 3.x (testes realizados na versão 3.6)
- Sistema Operacional: Windows (testes realizados no Windows 10)
<p></p>

---

## Execução

Para executar este simulador faz-se necessário a instalação do interpretador de Python 3.x. Neste trabalho utilizou-se a versão 3.6, mas acredita-se que o projeto seja compatível com qualquer versão superior à versão 3.0. As etapas abaixo apresentam uma explanação a respeito da execução do simulador. Assume-se que o interpretador já esteja instalado e que os arquivos com extensão .py estejam configurados para serem executadas, por padrão, pelo interpretador de Python.

1. Executar o programa Simulador.py
2. Na tela de boas vindas do programa, informar o caminho do arquivo de entrada contendo as referências às páginas.
    *  O caminho informado deve incluir a extensão .txt;
    * Se o arquivo estiver no mesmo diretório do programa (Simulador.py) basta informar o nome_do_arquivo.txt;
    * Se o arquivo estiver em outro diretório faz-se necessário informar o caminho completo.
3. Informar qual o esquema de mapeamento desejado para a simulação
    * Deverá ser informado um valor entre 1 e 3, indicando cada um dos esquemas de mapeamento implementados no simulador. Números fora deste intervalo não serão aceitos
4. Por fim, inserir o tamanho desejado para a cache.
    * Se o esquema de mapeamento desejado for o Associativo por conjunto, o tamanho da cache nesta simulação não poderá ser um número ímpar.
5. O programa irá executar a simulação de acordo com os parâmetros inseridos. 
    * Os resultados (estado da cache após cada leitura no arquivo de entrada e fração de acertos) são mostrados em tela e salvos em arquivo, dentro da pasta Outputs.
    * Para cada execução o simulador irá gerar um arquivo de saída cujo título será uma concatenação dos parâmetros da simulação, isto é, o esquema de mapeamento e o tamanho da cache. Ex.: Se a simulação considerar o Mapeamento Associativo com cache de tamanho 4, o arquivo de saída será: “Output_AssociativeMapping_Cache4.txt”
    * Caso haja a execução de uma outra simulação com os mesmos parâmetros, o arquivo de saída salvo previamente será sobrescrito, perdendo portanto, os dados da primeira simulação.
