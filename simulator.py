import random

class CacheSimulator:

    def __init__(self, pages_file, cache_size, mapping_scheme):
        pages = self.readMemoryAddress(pages_file)

        #Variáveis globais de auxilio/manipulação de dados
        self.cache = {}
        self.cache_size = cache_size
        self.pages_size = len(pages)
        self.all_statistics = ""
        self.SET_ASSOCIATIVE = "Set Associative"
        self.DIRECT = "Direct"

        if pages is not -1:
            self.start(mapping_scheme, pages)

    def start(self, mapping_scheme, pages):
        """
        Este método inicia a simulação. Se o esquema de mapeamento 'mapping_scheme' não for Mapeamento Direto, todas as
        4 políticas de substituição são consideradas.
        
        :param mapping_scheme: Esquema de mapeamento {"Direct", "Associative", "Set Associative"}
        :param pages: arquivo contendo as páginas lidas
        """

        output = "Outputs\Output_"  + mapping_scheme.replace(" ", "") +  "Mapping_Cache" + str(self.cache_size) + ".txt"
        with open(output, 'w') as self.f:

            print("\nStarting Simulation...\n", file=self.f)
            if mapping_scheme == "Direct":
                self.directMappedCache(pages)
            else:
                self.fifo(mapping_scheme, pages)
                self.lru(mapping_scheme,pages)
                self.lfu(mapping_scheme, pages)
                self.randon(mapping_scheme,pages)

            if self.all_statistics != "":
                print("\n\n============================================================================\nBRIEF:\n",self.all_statistics, file=self.f )

        with open(output, 'r') as result:
            print(*result.readlines())

        print('Simulação concluída! Arquivo "' + output +  '" gerado com sucesso!')

    def statistics(self, mapping_scheme,hits,misses, replacement_policy = ""):
        """
        Método que auxlia na impressão dos resultados. É chamado após o fim da execução de cada algoritmo de substituição
        e ao fim da execução de toda a simulação.
        
        :param mapping_scheme: Esquema de mapeamento {"Direct", "Associative", "Set Associative"}
        :param hits: Quantidade de hits para o mapeamento em questão
        :param misses: Quantidade de misses para o mapeamento em questão
        :param replacement_policy: Política de Substituição 
        """

        msg = "\n::::::: STATISTICS FOR "+ mapping_scheme.upper()+ " MAPPING - " + replacement_policy.upper() + " :::::::"
        msg+= "\nHits  : " + str(hits) + "/" + str(self.pages_size) + " = " +   str(round((hits/self.pages_size) * 100, 2)) + "%"
        msg+= "\nMisses: " + str(misses) + "/" + str(self.pages_size) + " = " + str(round((misses/self.pages_size) * 100, 2)) + "%"

        print(msg, "\nCache :", *self.cache, sep=" ", file=self.f)

        if mapping_scheme != self.DIRECT: self.all_statistics += msg + "\n"


    def readMemoryAddress(self, pages_file):
        """
        Método que realiza a leitura de um arquivo de páginas
        :param pages_file: Arquivo contendo as referências de páginas
        :return: objeto contendo a listagem das páginas lidas
        """
        try:
            with open(pages_file) as f:
                pages = f.read().splitlines()
        except:
            print("Invalid Directory")
            return -1

        try:
            pages = [int(x) if x != '' else "--" for x in pages]
            pages = list(filter(("--").__ne__, pages))
        except:
            print("Input file has invalid characters")

        return pages

    def prepareCache(self, mapping_scheme):
        """
        Prepara a cache, dado um esquema de mapemento.
        :param mapping_scheme: Esquema de mapeamento {"Direct", "Associative", "Set Associative"}
        """
        self.cache = []

        if mapping_scheme in ["Direct", "Associative"] :
            self.cache = ["--"] * self.cache_size
            self.set_size = self.cache_size

        elif mapping_scheme == self.SET_ASSOCIATIVE:
            if self.cache_size % 2 != 0:
                print("The cache size is not a power of 2. You can not use this mapping scheme.")
            else:
                self.set_size = 2
                sets = self.cache_size//self.set_size

                for i in range(sets):
                    self.cache.append(["--"] * self.set_size)

    def printTitle(self, title ):
        """
        Imprime o título de uma dada execução.
        :param title: Título
        """
        print("\n============================================================================", file=self.f)
        print(title.upper(), file=self.f)

    def printIteration(self, iteration_number, page, mapping_scheme, indexes, replacement_policy=" "):
        """
        Imprimir o estado da cache em cada iteração, isto é, após a leitura de cada página no arquivo de páginas
        :param iteration_number: Número da iteração 
        :param page: Páginas lida
        :param mapping_scheme: Esquema de Mapeamento
        :param indexes: posições da cache
        :param replacement_policy: Política de Substituição
        """

        if mapping_scheme == self.SET_ASSOCIATIVE:
            #Ajusta as posições da cache para serem exibidas de acordo com a quantidade de conjuntos.
            sets_number = self.cache_size // len(indexes)
            result  = ["      "] * (sets_number * 2 - 1)
            result[0::2] = list(range(sets_number))

            print("\nIteration", iteration_number, "- Page", page, "-", mapping_scheme.upper(),
              "MAPPING", replacement_policy, "\n      Address:", *result, "\nCache Content:", *self.cache, sep="\t", file=self.f)
        else:
            print("\nIteration", iteration_number, "- Page", page,"-", mapping_scheme.upper(),
              "MAPPING",replacement_policy, "\n      Address:", *indexes, "\nCache Content:", *self.cache, sep="\t", file=self.f)


    def fifo(self, mapping_scheme, pages):
        """
        Implementação do algoritmo de Substituição FIFO
        :param mapping_scheme: Esquema de mapeamento {"Direct", "Associative", "Set Associative"}
        :param pages: listagem das páginas lidas
        """
        self.prepareCache(mapping_scheme)
        self.printTitle(mapping_scheme +" MAPPING - FIFO:")

        indexes = list(range(self.set_size))
        hits, misses = 0, 0
        for index,page in enumerate(pages):

            # Encontra o conjunto onde a pagina referente a páginas (Caso Mapeamento Associativo por conjunto)
            if mapping_scheme == self.SET_ASSOCIATIVE:
                position = page % len(self.cache)
                cache_to_load = self.cache[position]
                flat_cache = [item for sublist in self.cache for item in sublist]
            else:
                cache_to_load = self.cache
                flat_cache = self.cache

            #Se a página esta na cache é um HIT, caso contrário, um MISS e a página é carregada
            if page in flat_cache:
                hits += 1
            else:
                misses +=1
                self.loadPage(cache_to_load, page)
            self.printIteration(index+1, page, mapping_scheme, indexes, replacement_policy="FIFO")
        self.statistics(mapping_scheme, hits, misses, replacement_policy="FIFO")


    def lru(self, mapping_scheme, pages):
        """
        Implementação do Algoritmo de Substituição LRU
        :param mapping_scheme: Esquema de mapeamento {"Direct", "Associative", "Set Associative"}
        :param pages: listagem das páginas lidas
        """
        self.prepareCache(mapping_scheme)
        self.printTitle(mapping_scheme + " MAPPING - LRU:")

        hits, misses = 0, 0
        indexes = list(range(self.set_size))
        for index,page in enumerate(pages):

            # Encontra o conjunto onde a pagina referente a páginas (Caso Mapeamento Associativo por conjunto)
            if mapping_scheme == self.SET_ASSOCIATIVE:
                position = page % len(self.cache)
                cache_to_load = self.cache[position]
                flat_cache = [item for sublist in self.cache for item in sublist]
            else:
                cache_to_load = self.cache
                flat_cache = self.cache

            # Se a página esta na cache é um HIT, caso contrário, um MISS e a página é carregada
            if page in flat_cache:
                hits += 1

                # Se a página já esta na cache, ela é movida para o extremo direito da lista
                # Desse modo, sempre faz-se a remoção no extremo esquerdo (ínicio) da lista,
                # sendo nas posições mais à direita os de maiores recência
                del cache_to_load[cache_to_load.index(page)]
                if "--" in cache_to_load:
                    cache_to_load[cache_to_load.index("--")] = page
                    cache_to_load.append("--")
                else:
                    cache_to_load.append(page)
            else:
                misses += 1

                self.loadPage(cache_to_load, page)
            self.printIteration(index + 1, page, mapping_scheme, indexes, replacement_policy="LRU")
        self.statistics(mapping_scheme, hits, misses, replacement_policy="LRU")


    def lfu(self, mapping_scheme, pages):
        """
        Implementação do Algoritmo de Substituição LFU
        :param mapping_scheme: Esquema de mapeamento {"Direct", "Associative", "Set Associative"}
        :param pages: listagem das páginas lidas
        """
        self.prepareCache(mapping_scheme)
        self.printTitle(mapping_scheme + " MAPPING - LFU:")

        hits, misses = 0, 0
        counters = [0]* self.set_size
        indexes = list(range(self.set_size))
        for index,page in enumerate(pages):

            # Encontra o conjunto onde a pagina referente a páginas (Caso Mapeamento Associativo por conjunto)
            if mapping_scheme == self.SET_ASSOCIATIVE:
                position = page % len(self.cache)
                cache_to_load = self.cache[position]
                flat_cache = [item for sublist in self.cache for item in sublist]
            else:
                cache_to_load = self.cache
                flat_cache = self.cache

            # Se a página esta na cache é um HIT, caso contrário, um MISS e a página é carregada
            if page in flat_cache:
                hits += 1
            else:
                misses += 1
                self.loadPage(cache_to_load,page,"lfu", counters)
            self.printIteration(index + 1, page, mapping_scheme, indexes, replacement_policy="LFU" )
        counters[cache_to_load.index(page)] += 1
        self.statistics(mapping_scheme, hits, misses, replacement_policy="LFU")


    def randon(self, mapping_scheme, pages):
        """
        Implementação do Algoritmo de Substituição Randon
        :param mapping_scheme: Esquema de mapeamento {"Direct", "Associative", "Set Associative"}
        :param pages: listagem das páginas lidas
        """
        self.prepareCache(mapping_scheme)
        self.printTitle(mapping_scheme + " MAPPING - RANDOM:")

        hits, misses = 0, 0
        indexes = list(range(self.set_size))
        for index,page in enumerate(pages):
            # Encontra o conjunto onde a pagina referente a páginas (Caso Mapeamento Associativo por conjunto)
            if mapping_scheme == self.SET_ASSOCIATIVE:
                position = page % len(self.cache)
                cache_to_load = self.cache[position]
                flat_cache = [item for sublist in self.cache for item in sublist]
            else:
                cache_to_load = self.cache
                flat_cache = self.cache

            # Se a página esta na cache é um HIT, caso contrário, um MISS e a página é carregada
            if page in flat_cache:
                hits += 1
            else:
                misses +=1
                self.loadPage(cache_to_load, page, "random")
            self.printIteration(index + 1, page, mapping_scheme, indexes, replacement_policy="RANDOM")
        self.statistics(mapping_scheme, hits, misses, replacement_policy="RANDOM")


    def directMappedCache(self, pages):
        """
        Implementação do Esquema de Mapeamento Direto
        :param pages: listagem das páginas lidas
        """
        self.prepareCache(self.DIRECT)
        self.printTitle(self.DIRECT + " MAPPING")

        hits, misses = 0,0
        mapping_scheme = "Direct"
        indexes = list(range(self.set_size))
        for index,page in enumerate(pages):
            position = page % self.cache_size

            # Se a página esta na cache é um HIT, caso contrário, um MISS e a página é carregada
            if self.cache[position] == page:
                hits += 1
            else:
                misses += 1
                self.cache[position] = page
            self.printIteration(index + 1, page, mapping_scheme, indexes)
        self.statistics(mapping_scheme,hits,misses)


    def loadPage(self, cache, page, replacement_policy = "default", counters = None):
        """
        Carrega a página na memória cache considerando a política de substituição.
        :param cache: Memória Cache. Se esquema de mapeamento igual a Associativo por conjunto, cache é igual ao conjunto 
        ao qual a página deve pertencer
        :param page: Página a ser carregada na cache
        :param replacement_policy: Política de Substituição
        :param counters: contadores da frequência de cada página. Excluido para replacement_policy igual a "lfu"
        """

        #Se não cache possui posições vazias
        if "--" not in cache:
             #Elimina a primeira posição da cache e adiciona a página no final da lista.
             if replacement_policy == "default":
                del cache[0]
                cache.append(page)

             #Seleciona aleatoriamente uma posição e carrega a página nesta posição
             elif replacement_policy == "random":
                 position = random.randint(0, len(cache) - 1)
                 cache[position] = page

             #Encontra o(s) índice(s) da(s) página(s) que possui(em) menor frequência e então substitui a página
             #Se há mais de uma página na cache com a mesma recência a página a ser removida é escolhida aleatóriamente
             elif replacement_policy == "lfu" and counters is not None:
                 min_indexes = [index for index, value in enumerate(counters) if value == min(counters)]
                 index = random.choice(min_indexes)
                 counters[index] = 0
                 cache[index] = page

        #Caso exista posições vázias na cache, substitui a primeira posição vazia pela página em questão
        else:
            cache[cache.index("--")] = page


if __name__ == '__main__':
    print(":::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::\n"
          "::::::::::::::::::::::    CACHE SIMULATOR    ::::::::::::::::::::::\n"
          ":::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::\n")

    pages_file = input("Please insert the full path to file containing the page references:\nAnswer:")

    mappring_scheme = None
    while mappring_scheme not in ['1','2','3']:
        mappring_scheme = input("\nChoose a mapping scheme:\n1) Direct Mapping\n2) Associative Mapping\n3) Set Associative Mapping\nAnswer:")
        if mappring_scheme not in ['1','2','3']: print("Invalid Option")

    mappring_scheme = int(mappring_scheme)
    cache_size = int(input("\nType the cache size:\nAnswer:"))

    if mappring_scheme == 1: mappring_scheme = "Direct"
    elif mappring_scheme == 2: mappring_scheme = "Associative"
    elif mappring_scheme == 3: mappring_scheme = "Set Associative"

    CacheSimulator(pages_file,cache_size, mappring_scheme)